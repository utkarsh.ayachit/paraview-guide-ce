include(UseLATEX)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../ParaView/menukeys.sty DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

add_latex_document(
  ParaViewCatalystUsersGuide.tex
  INPUTS
    Acknowledgements.tex
    Introduction.tex
    UserSection.tex
    DeveloperSection.tex
    BuildSection.tex
    Examples.tex
    References.tex
    Appendix.tex
    multicols.sty
  IMAGE_DIRS Images
  DEFAULT_PDF
  USE_INDEX
  MANGLE_TARGET_NAMES
)
