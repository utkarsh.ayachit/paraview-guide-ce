In this chapter, we will introduce various ways of saving visualization results
in \ParaView. Results generated throughout the visualization process not
only include the images and the rendering results, but also include the datasets generated by filters,
the scene representations that will be imported into other rendering applications, and
the movies generated from animations.

\section{Saving datasets}
\label{sec:SavingDatasets}
You can save the dataset produced by any pipeline module in \ParaView, including
sources, readers, and filters. To save the dataset in \paraview, begin
by selecting the pipeline module in the \ui{Pipeline} browser to make it the
active source. For modules with multiple output ports, select the output port
producing the dataset of interest. To save the dataset, use the \menu{File > Save Data}
menu or the \icon{Images/pqSave32.png} button in the \ui{Main Controls}
toolbar. You can also use the keyboard shortcut \keys{\ctrl+S} (or \keys{\cmdmac+S}).
The \ui{Save File} dialog (Figure~\ref{fig:SaveFileDialog})
will allow you to select the filename and the file format.
The available list of file formats depends on the type of the dataset you are
trying to save.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/SaveFileDialog.png}
\caption{\ui{Save File} dialog in \paraview.}
\label{fig:SaveFileDialog}
\end{center}
\end{figure}

On accepting a filename and file format to use, \paraview may show
the \ui{Configure Writer} dialog (Figure~\ref{fig:ConfigureWriterDialogCSV}).
This dialog allows you to further customize the
writing process. The properties shown in this dialog depend on the selected file
format and range from enabling you to \ui{Write All Time Steps}, to selecting
the attributes to write in the output file.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ConfigureWriterDialogCSV.png}
\caption{\ui{Configure Writer} dialog in \paraview shown when saving
a dataset as a csv file.}
\label{fig:ConfigureWriterDialogCSV}
\end{center}
\end{figure}

In \pvpython too, you can save the datasets as follows:

\begin{python}

# Saving the data using the default properties for
# the used writer, if any.
>>> SaveData("sample.csv", source)

# the second argument is optional, and refers to the pipeline module
# to write the data from. If none is specified the active source is used.

# To pass parameters to configure the writer
>>> SaveData("sample.csv", source,
             Precision=2,
             FieldAssociation='Cells')
\end{python}

\pvpython will pick a writer based on the file extension and the dataset type
selected for writing, similar to what it does in \paraview.
Admittedly, it can be tricky to figure out what options are available for the
writer. The best way is to use the \emph{Python tracing} capabilities in
\paraview and to use the generated sample script as a
reference (Section~\ref{sec:PythonTracing}).
Make sure you use a similar type of dataset and the same file format as
you want to use in your Python script, when tracing, to avoid runtime issues.

\section{Saving rendered results}

Views that render results (this includes almost all of the views, except
\ui{SpreadSheet View}) support saving images (or screenshots) in one of the
standard image formats (png, jpeg, tiff, bmp, ppm).
Certain views also support exportings the results in several formats such as
pdf, x3d, and vrml.

\subsection{Saving screenshots}

To save the render image from a view in \paraview,
use the \menu{File > Save Screenshot} menu
option. This will pop up the \ui{Save Screenshot Options} dialog
(Figure~\ref{fig:SaveScreenshotOptions}). This dialog allows you to select
various image parameters, such as image resolution and image quality (which
depends on the file format chosen). The dialog also provides an option to change
the color palette to use to save the image using \ui{Override Color Palette}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/SaveScreenshotOptions.png}
\caption{The \ui{Save Screenshot Options} dialog, which is used to customize saving
screenshots in \paraview}
\label{fig:SaveScreenshotOptions}
\end{center}
\end{figure}

By default, \paraview will save rendered results from the active
view. Optionally, you can save an image comprising of all the views layed out
exactly as on the screen by unchecking the \ui{Save only selected view} button.

When saving mutliple views, you can customize the border to use between views,
as well as the color to use for that border, from the \ui{General} tab in
\ui{Settings} dialog (\menu{Edit > Settings}), as shown in the
Figure~\ref{fig:ScreenshotOptionsSettingsDialog}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/ScreenshotOptionsSettingsDialog.png}
\caption{\ui{Screenshot Options} in the \ui{Settings} dialog allow you to set
the size in pixels and the color for the border to use to separate views with
saving multiple views in a screenshot.}
\label{fig:ScreenshotOptionsSettingsDialog}
\end{center}
\end{figure}


\begin{didyouknow}
  You can save high-resolution images at a resolution higher than your monitor by
  specifying a higher resolution in the \ui{Save Screenshot Options} dialog.
  \paraview will use tiling and magnification internally to produce
  an image of the requested size.
\end{didyouknow}

Saving a screenshot is also possible in \paraview.

\begin{python}
# Save a screenshot from a specific view.
>>> myview = GetActiveView()
>>> SaveScreenshot("sample.png", view=myview)

# To specify magnification and quality, use the following form:
>>> SaveScreenshot("sample2X.png", view=myview, magnification=2, quality=100)

# You can skip the 'view' parameter if you simply want to save the active view.
>>> SaveScreenshot("sample.png")

# To save multiple views, you can use the following form:

# Get the layout in which a specific view is placed
# if 'myview' is not provided, active view will be used.
>>> layout = GetLayout(myview)

>>> SaveScreenshot("multiview.png", layout=layout)
\end{python}

As always, you can use \emph{Python tracing} in \paraview to trace
the exact form of the method to use to save a specific screenshot image.

\subsection{Exporting scenes}

When available, you can export a visualization in a view in several of the
supported formats using the \menu{File > Export View} menu option in
\paraview. For a
\ui{Render View} (or similar), the available formats include
EPS, PDF, PS, SVG, POV, VRML, WebGL, X3D, and X3DB. On selecting a file
as which to export, \paraview may pop up an \ui{Export Options} dialog that
allows you to set up parameters for the exporter, similar to saving datasets
(Section~\ref{sec:SavingDatasets}).

In addition, from \paraview, exporting takes the following form (again,
just use \emph{Python trace} to figure out the proper form -- that's the
easiest way).

\begin{python}
>>> myview = GetActiveView()
>>> ExportView('/tmp/sample.svg', view=myview,
               Plottitle='ParaView GL2PS Export',
               Compressoutputfile=1)
# the arguments after 'view' depend on the exporter selected.
\end{python}

% \begin{didyouknow}
%  To save images to be included in publications, you have two options, save a
%  high resolution rasterized images as png, bmp, etc. or export the image in one
%  of the vector formats such as pdf, eps. For details on how this works and how
%  to fine tune it, refer to Section~\ref{}.\fixme{fix reference}
% \end{didyouknow}

\section{Saving animation}

%\subsection{Saving animation images}

In \paraview, you use the \menu{File > Save Animation} menu option to
save the animation results as a movie or as a series of images. This will pop up the
\ui{Animation Settings Dialog}. Here, you can pick the \ui{Frame Rate} in
frames-per-second for the generated movie (if applicable for the chosen file
format) and the total \ui{Number Of Frames} to render. The \ui{Animation
Duration (sec)} field tells you how long the movie file would play given the
specified settings. Animation always saves out all the views currently visible
in \paraview. Thus, if you maximize a view, the animation will only
include the maximized view. When saving multiple views, the \ui{Screenshot
Options} in the \ui{Settings} (Figure~\ref{fig:ScreenshotOptionsSettingsDialog})
are used to add borders between views. \ui{Frame range} can be used to specify
the range of frame over which to animate, thus allowing you to skip parts of the total
animation in the saved file(s).

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/AnimationSettingsDialog.png}
\caption{The \ui{Animation Settings Dialog} in \paraview, which is used to
customize saving of animation files.}
\label{fig:AnimationSettingsDialog}
\end{center}
\end{figure}

The available file formats include avi and ogg (when available) movie formats, as
well as image formats such png, jpg, and tif. If saving as
images, \ParaView will generate a series of image files sequentially numbered
using the frame number as a suffix to the specified filename.

In \paraview, you can save an animation using \py{WriteAnimation}.

\begin{python}
>>>  WriteAnimation('animation.avi', Magnification=1, FrameRate=15.0, Compression=True)
\end{python}

% \subsection{Saving animation geometries}
% We should add documentation about saving animation geometries, but that's not
% that critical, so we'll leave it for later.

\section{Saving state}

Besides saving the results produced by your visualization setup, you can save
the state of the visualization pipeline itself, including all the pipeline
modules, views, their layout, and their properties. This is referred to as the
application state or, just, state. In \paraview, you can save the
state using the \menu{File > Save State\ldots} menu option. Conversely, to load a saved
state file, you can use \menu{File > Load State\ldots}.

There are two types of state files that you can save in \paraview:
\emph{ParaView state file (*.pvsm)} and \emph{Python state file (*.py)}. The
PVSM files are XML-based text files that are human and machine readable,
although not necessarily a novice user human friendly. However, if you don't
plan to read and make sense of the state files, PVSM is the most robust and
reliable way to save the application state. For those who want to save the state and then
modify it manually, using Python state files may be better, as using
Python trace simply traces the actions that you perform in the UI as a
Python script. Python state files, on the other hand, save the entire current state of the
application as a Python script that you can use in \paraview or the
\ui{Python Shell}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.5\linewidth]{Images/SaveStateDialog.png}
\caption{The \ui{Save State File} dialog in \paraview.}
\label{fig:SaveStateDialog}
\end{center}
\end{figure}

To load a PVSM file, you use the \menu{File > Load State\ldots} menu. Note that loading
a state file will affect the current visualization state. To load the Python
state file in \paraview, you will need to use the \ui{Python Shell},
which is accessible from the \menu{Tools} menu.

You can save/load the PVSM state file in \pvpython as follows:

\begin{python}
>>> from paraview.simple import *

# Save the PVSM state file. Currently, this doesn't support
# saving Python state files.
>>> SaveState("sample.pvsm")

# To load a PVSM state file.
>>> LoadState("sample.pvsm")
\end{python}
